﻿using Mastermind.Core.Game;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind
{
    class Mastermind
    {
        /// <summary>
        /// Entry point for the app.  This will get the configurable value for the number of guesses,
        /// then it will instansiate the main app and call the Run() function.  Everything else
        /// will be taken care of in that code.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // probably want to move this to an .ini file
            var numberOfGuesses = Convert.ToInt32(ConfigurationManager.AppSettings["numberOfGuesses"]);
            var mastermindGame = new MastermindApp();
            mastermindGame.Run(numberOfGuesses);
        }
    }
}
