﻿
namespace Mastermind.Models.Enums
{
    public enum GameState
    {
        Solved,
        WrongGuess,
        Exit,
        InvalidInput
    }
}
