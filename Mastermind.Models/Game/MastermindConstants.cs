﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind.Models.Game
{
    public static class MastermindConstants
    {
        public const int CODE_LENGTH = 4;
        public const int CODE_DIGIT_MAX = 7;
        public const int CODE_DIGIT_MIN = 1;

        public const string EXIT = "exit";
        public const string E = "e";
        public const string ASTERISK_BREAK = "****************************************";
        public const string LOSE_MESSAGE = "You Lose :(";
        public const string WIN_MESSAGE = "You solved it!";
    }
}
