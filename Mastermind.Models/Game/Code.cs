﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind.Models.Game
{
    public class Code
    {
        public List<int> Digits { get; set; }

        public Code()
        {
            Digits = new List<int>();
        }

        public Code(List<int> digits)
        {
            Digits = digits;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            
            foreach (var digit in Digits)
            {
                builder.Append(digit);
            }

            return builder.ToString();
        }
    }
}
