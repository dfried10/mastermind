﻿using System.Collections.Generic;

namespace Mastermind.Models.Game
{
    public class Result
    {
        public List<Match> NumberPositionMatches { get; set; }
        public List<Match> NumberMatches { get; set; }        
        public string LastMatchString { get; set; }

        public Result()
        {
            NumberPositionMatches = new List<Match>();
            NumberMatches = new List<Match>();            
        }
    }
}
