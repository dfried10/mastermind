﻿using Mastermind.Models.Enums;

namespace Mastermind.Models.Game
{
    public class Match
    {
        public MatchType MatchType { get; set; }
        public int GuessPosition { get; set; }
        public int CodePostition { get; set; }
        public int Value { get; set; }
    }    
}
