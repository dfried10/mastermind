﻿using Mastermind.Core.Engine;
using Mastermind.Models.Game;
using Mastermind.Models.Enums;
using System;
using System.Text;

namespace Mastermind.Core.Game
{
    public interface IMastermindGame
    {
        Result TurnResult { get; set; }
        Code Guess { get; set; }
        Code MasterCode { get; set; }

        IInputParser InputParserDI { get; set; }                
        IRulesEngine RulesEngineDI { get; set; }

        GameState ProcessInput(string inputValue);
    }

    public class MastermindGame : IMastermindGame
    {
        public Result TurnResult { get; set; }
        public Code MasterCode { get; set; }
        public Code Guess { get; set; }

        //dependency injection
        private IInputParser _inputParser = new InputParser();
        public IInputParser InputParserDI
        {
            get { return _inputParser; }
            set { _inputParser = value; }
        }

        private IRulesEngine _rulesEngine = new RulesEngine();
        public IRulesEngine RulesEngineDI 
        {
            get { return _rulesEngine; }
            set { _rulesEngine = value; }
        }
        
        // members
        private Random _randomNum;       

        public MastermindGame()
        {
            TurnResult = new Result();
            MasterCode = new Code();
            _randomNum = new Random();

            // on construction create the game code
            GenerateGameCode();
        }

        /// <summary>
        /// Initializes the game's code the user is trying to crack
        /// </summary>
        private void GenerateGameCode()
        {
            for (int i = 0; i < MastermindConstants.CODE_LENGTH; i++)
            {
                MasterCode.Digits.Add(_randomNum.Next(MastermindConstants.CODE_DIGIT_MIN, MastermindConstants.CODE_DIGIT_MAX));
            }
        }

        /// <summary>
        /// Takes the input from the user and executes the logic to check if the 
        /// guess is correct.  Returns the game state for the main app look to take
        /// action against.
        /// </summary>
        /// <param name="inputValue">User's code guess</param>
        /// <returns>GameState based on results</returns>
        public GameState ProcessInput(string inputValue)
        {
            // check if exiting first
            if (inputValue.ToLower() == MastermindConstants.EXIT || inputValue.ToLower() == MastermindConstants.E)
            {
                return GameState.Exit;
            }

            // verify the input contains no letters and is only 4 digits
            if (!InputParserDI.IsInputValid(inputValue))
            {
                return GameState.InvalidInput;
            }            

            // parse the value and give the guess to the engine
            Guess = InputParserDI.ParseLineForGuess(inputValue);

            // compare against the code to get a collection of matches            
            TurnResult = RulesEngineDI.CompareGuessAgainstCode(Guess, MasterCode);

            // check the results now, if it wasn't a solved puzzle then set the string
            if (TurnResult.NumberPositionMatches.Count == MastermindConstants.CODE_LENGTH)
            {
                return GameState.Solved;
            }

            // finish setting the results for printing
            BuildAnswerString();
            
            return GameState.WrongGuess;
        }

        /// <summary>
        /// Creates the result string when the user makes an incorrect guess.
        /// </summary>
        private void BuildAnswerString()
        {
            StringBuilder builder = new StringBuilder();

            // add the exact matches first
            for (int i = 0; i < TurnResult.NumberPositionMatches.Count; i++)
            {
                builder.Append("+");
            }

            // add the number matches
            for (int j = 0; j < TurnResult.NumberMatches.Count; j++)
            {
                builder.Append("-");
            }

            //set the value on the result
            TurnResult.LastMatchString = builder.ToString();        
        }
    }
}
