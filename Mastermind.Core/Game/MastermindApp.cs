﻿using Mastermind.Core.Display;
using Mastermind.Models.Game;
using Mastermind.Models.Enums;

namespace Mastermind.Core.Game
{
    /// <summary>
    /// What are our steps for the game?
    /// 1. Build the initial game state.
    /// 2. Setup game loop
    ///     - Reads last input from user
    ///     - Executes logic against the Code
    ///     - Checks if correct, 
    ///         if good then prints end game, asks if they want to play again
    ///         else prompts user for next input
    ///     - Once guesses are uses up, exit loop
    /// </summary>
    public class MastermindApp
    {
        private bool _running = true;

        // Dependency Injection
        private IMastermindGame _mastermindGame = new MastermindGame();
        public IMastermindGame MastermindGameDI
        {
            get { return _mastermindGame; }
            set { _mastermindGame = value; }
        }

        private IMastermindIO _mastermindIO = new MastermindIO();
        public IMastermindIO MastermindIODI
        {
            get { return _mastermindIO; }
            set { _mastermindIO = value; }
        }

        public void Run(int numberOfGuesses)
        {
            // quick init check to make sure they can play
            if (numberOfGuesses <= 0)
            {
                MastermindIODI.Print("You have no guesses. Check your configuration and try again.");
                return;
            }            

            // start game running
            while (_running)
            {
                MastermindIODI.PrintLines(
                    MastermindConstants.ASTERISK_BREAK,
                    string.Format("Welcome to Mastermind! You have {0} attempts to crack the code. Good luck!", numberOfGuesses),
                    string.Format("To make a guess: enter {0} digits with no spaces. ", MastermindConstants.CODE_LENGTH),
                    string.Format("To exit the game: enter {0} or {1} and press enter", MastermindConstants.EXIT, MastermindConstants.E),
                    "If you enter a code with letters, or it is the incorrect length it will not be counted as an attempt.",
                    MastermindConstants.ASTERISK_BREAK
                    );
                                
                //turn loop
                for (int i = 0; i < numberOfGuesses; i++)
                {
                    MastermindIODI.Print("Enter your guess: ");
                    var inputGuess = MastermindIODI.GetNextInput();
                    var gameCode = MastermindGameDI.ProcessInput(inputGuess);

                    if (gameCode == GameState.Solved)
                    {
                        MastermindIODI.PrintLines(MastermindConstants.WIN_MESSAGE);
                        break;
                    }
                    else if (gameCode == GameState.WrongGuess)
                    {
                        MastermindIODI.PrintLines(MastermindGameDI.TurnResult.LastMatchString);

                        if (i == numberOfGuesses - 1)
                        {
                            // if we made it here, we lost
                            MastermindIODI.PrintLines(MastermindConstants.LOSE_MESSAGE, string.Format("The solution was: {0}", MastermindGameDI.MasterCode));                            
                        }
                    }
                    else if (gameCode == GameState.InvalidInput) 
                    {
                        //don't count this guess against them
                        --i;
                        MastermindIODI.PrintLines("You entered the incorrect number of characters. Please try again.");
                    }
                    else if (gameCode == GameState.Exit)
                    {
                        _running = false;
                        MastermindIODI.PrintLines("Exiting game...");
                        break;
                    }                    
                }                

                // Prompt user to close console at end of game
                MastermindIODI.PrintLines(MastermindConstants.ASTERISK_BREAK,
                    "Press Enter to close console",
                    MastermindConstants.ASTERISK_BREAK);
                MastermindIODI.GetNextInput();                
                _running = false;
            }         
        }
    }
}
