﻿using Mastermind.Models.Game;
using Mastermind.Models.Enums;
using System.Collections.Generic;

namespace Mastermind.Core.Engine
{
    public interface IRulesEngine
    {
        Result CompareGuessAgainstCode(Code guess, Code masterCode);
    }

    public class RulesEngine : IRulesEngine
    {
        /// <summary>
        /// Compares the user's guess against the master code.  The logic is as follows:
        ///     -Check for any exact matches, if there is an exact match then create the match
        ///         and remove those records from each collection
        ///     -Then look for fuzzy matches, if there is a match in the master code, create
        ///         the match and remove the number from the master code
        ///     -Return the <code>Result</code> which has the collection of matches
        /// </summary>
        /// <param name="guess">User's guess code</param>
        /// <param name="masterCode">Game's master code</param>
        /// <returns>Result object containg the matches</returns>
        public Result CompareGuessAgainstCode(Code guess, Code masterCode)
        {
            Result result = new Result();
            Code localMasterCode = new Code
            {
                Digits = new List<int>(masterCode.Digits)
            };

            // check for exact matches and remove them, they're solved
            for (int i = guess.Digits.Count - 1; i > -1; i--)
            {
                var guessDigit = guess.Digits[i];
                var codeDigit = localMasterCode.Digits[i];

                if (guessDigit == codeDigit)
                {
                    result.NumberPositionMatches.Add(new Match
                    {
                        MatchType = MatchType.NumberPosition,
                        GuessPosition = i,
                        CodePostition = i,
                        Value = guessDigit
                    });

                    guess.Digits.RemoveAt(i);
                    localMasterCode.Digits.RemoveAt(i);
                }
            }

            // check the remaing digits for fuzzy matches
            foreach (int guessDigit in guess.Digits)
            {                
                var match = localMasterCode.Digits.FindIndex(d => d == guessDigit);

                // If the value is in the master code, then we add a fuzzy match
                // then remove the number from the code so we don't duplicate it
                if (match != -1)
                {
                    result.NumberMatches.Add(new Match
                    {
                        MatchType = MatchType.Number,
                        Value = guessDigit
                    });

                    localMasterCode.Digits.RemoveAt(match);
                }
            }

            return result;
        }        
    }
}
