﻿using Mastermind.Models.Game;
using System;

namespace Mastermind.Core.Engine
{
    public interface IInputParser
    {
        Code ParseLineForGuess(string consoleLine);
        bool IsInputValid(string consoleLine);
    }

    public class InputParser : IInputParser
    {
        private const int MIN_INPUT_LENGTH = 4;

        public InputParser()
        {
        }

        public Code ParseLineForGuess(string consoleLine)
        {         
            Code guess = new Code();
            foreach (char digit in consoleLine)
            {
                guess.Digits.Add((int)Char.GetNumericValue(digit));
            }

            return guess;
        }

        public bool IsInputValid(string consoleLine)
        {
            if (consoleLine.Length != MIN_INPUT_LENGTH)
            {
                return false;
            }

            int n;
            if (!int.TryParse(consoleLine, out n))
            {
                return false;
            }

            return true;
        }
    }
}
