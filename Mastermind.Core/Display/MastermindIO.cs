﻿using System;

namespace Mastermind.Core.Display
{
    public interface IMastermindIO
    {
        void PrintLines(params string[] line);
        void Print(string toPrint);
        string GetNextInput();
    }

    public class MastermindIO : IMastermindIO
    {
        public void PrintLines(params string[] lines)
        {
            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
        }

        public void Print(string toPrint)
        {
            Console.Write(toPrint);
        }

        public string GetNextInput()
        {
            return Console.ReadLine();
        }
    }
}
