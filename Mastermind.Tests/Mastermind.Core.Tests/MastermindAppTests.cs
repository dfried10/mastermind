﻿using Mastermind.Core.Display;
using Mastermind.Core.Game;
using Mastermind.Models.Enums;
using Mastermind.Models.Game;
using Moq;
using NUnit.Framework;

namespace Mastermind.Tests.Mastermind.Core.Game
{
    [TestFixture]
    public class MastermindAppTests
    {
        private MastermindApp _mastermindApp;
        private Mock<IMastermindGame> _mastermindGameMock;
        private Mock<IMastermindIO> _mastermindIOMock;

        [SetUp]
        public void Setup()
        {
            _mastermindApp = new MastermindApp();
            _mastermindGameMock = new Mock<IMastermindGame>();
            _mastermindIOMock = new Mock<IMastermindIO>();
        }

        /*
         * Test Cases:
         * Run()
         *  1. Invalid number of guesses from Main()
         *  2. Game solved on the first try, breaks
         *  3. Enter 'exit' code, breaks         
         */

        [TestCase]
        public void Run_InvalidNumberOfGuessFromMain_PrintsMessageAndBreaks()
        {
            //Arrange
            var numOfGuesses = 0;
            var printMessage = "You have no guesses. Check your configuration and try again.";

            _mastermindIOMock.Setup(output => output.Print(printMessage));

            _mastermindApp.MastermindIODI = _mastermindIOMock.Object;

            //Act
            _mastermindApp.Run(numOfGuesses);

            //Assert
            _mastermindIOMock.Verify(o => o.Print(printMessage));
        }

        [TestCase]
        public void Run_GameSolvedOnFirstTry_PrintsGameSolvedMessage()
        {
            //Arrange
            var numOfGuesses = 6;
            var inputGuess = "1234";
            var printMessage = MastermindConstants.WIN_MESSAGE;

            _mastermindIOMock.Setup(output => output.PrintLines(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _mastermindIOMock.Setup(output => output.PrintLines(printMessage));
            _mastermindIOMock.Setup(output => output.Print("Enter your guess: "));
            _mastermindIOMock.Setup(output => output.GetNextInput())
                .Returns(inputGuess);

            _mastermindGameMock.Setup(game => game.ProcessInput(inputGuess))
                .Returns(GameState.Solved);

            _mastermindApp.MastermindIODI = _mastermindIOMock.Object;
            _mastermindApp.MastermindGameDI = _mastermindGameMock.Object;

            //Act
            _mastermindApp.Run(numOfGuesses);

            //Assert
            _mastermindIOMock.VerifyAll();
            _mastermindGameMock.VerifyAll();
        }

        [TestCase]
        public void Run_ExitCodeEntered_QuitsLoopWithExitCode()
        {
            //Arrange
            var numOfGuesses = 6;
            var inputGuess = "exit";
            var printMessage = "Exiting game...";

            _mastermindIOMock.Setup(output => output.PrintLines(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _mastermindIOMock.Setup(output => output.PrintLines(printMessage));
            _mastermindIOMock.Setup(output => output.Print("Enter your guess: "));
            _mastermindIOMock.Setup(output => output.GetNextInput())
                .Returns(inputGuess);

            _mastermindGameMock.Setup(game => game.ProcessInput(inputGuess))
                .Returns(GameState.Exit);

            _mastermindApp.MastermindIODI = _mastermindIOMock.Object;
            _mastermindApp.MastermindGameDI = _mastermindGameMock.Object;

            //Act
            _mastermindApp.Run(numOfGuesses);

            //Assert
            _mastermindIOMock.VerifyAll();
            _mastermindGameMock.VerifyAll();
        }
    }
}
