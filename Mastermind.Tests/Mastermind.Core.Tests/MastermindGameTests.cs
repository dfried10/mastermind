﻿using Mastermind.Core.Engine;
using Mastermind.Core.Game;
using Mastermind.Models.Enums;
using Mastermind.Models.Game;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using Match = Mastermind.Models.Game.Match;

namespace Mastermind.Tests.Mastermind.Core.Tests
{
    [TestFixture]
    public class MastermindGameTests
    {
        private MastermindGame _mastermindGame;
        private Mock<IRulesEngine> _rulesEngineMock;
        private Mock<IInputParser> _inputParserMock;

        [SetUp]
        public void Setup()
        {
            _mastermindGame = new MastermindGame();
            _rulesEngineMock = new Mock<IRulesEngine>();
            _inputParserMock = new Mock<IInputParser>();
        }
        
        /*
         * Test cases:
         * 1. Standard, solved, 4 digit input
         * 2. Invalid input
         * 3. Wrong guess
         */

        [TestCase]
        public void ProcessInput_Standard4DigitInputSolved_ReturnsGameSolved()
        {
            //Arrange
            string userGuess = "1234";
            Result result = new Result
            {
                NumberPositionMatches = new List<Match>(4) { new Match(), new Match(), new Match(), new Match()}
            };

            _inputParserMock.Setup(parser => parser.IsInputValid(userGuess))
                .Returns(true);
            _inputParserMock.Setup(parser => parser.ParseLineForGuess(userGuess))
                .Returns(new Code());

            _rulesEngineMock.Setup(engine => engine.CompareGuessAgainstCode(It.IsAny<Code>(), It.IsAny<Code>()))               
                .Returns(result);

            _mastermindGame.InputParserDI = _inputParserMock.Object;
            _mastermindGame.RulesEngineDI = _rulesEngineMock.Object;

            //Act
            var gameState = _mastermindGame.ProcessInput(userGuess);

            //Assert
            Assert.AreEqual(GameState.Solved, gameState);
        }

        [TestCase]
        public void ProcessInput_5DigitGuess_ReturnsInvalidGuessGameState()
        {
            //Arrange
            var inputValue = "12345";

            _inputParserMock.Setup(parser => parser.IsInputValid(inputValue))
                .Returns(false);

            _mastermindGame.InputParserDI = _inputParserMock.Object;

            //Act
            var gameState = _mastermindGame.ProcessInput(inputValue);

            //Assert
            Assert.AreEqual(GameState.InvalidInput, gameState);
        }

        [TestCase]
        public void ProcessInput_WrongGuess_ReturnsWrongGuessGameState()
        {
            //Arrange
            var inputValue = "1234";
            var fakeResult = new Result
            {
                NumberPositionMatches = new List<Match> { new Match() }
            };

            _inputParserMock.Setup(parser => parser.IsInputValid(inputValue))
                .Returns(true);
            _inputParserMock.Setup(parser => parser.ParseLineForGuess(inputValue))
                .Returns(new Code());

            _rulesEngineMock.Setup(engine => engine.CompareGuessAgainstCode(It.IsAny<Code>(), It.IsAny<Code>()))
                .Returns(fakeResult);

            _mastermindGame.InputParserDI = _inputParserMock.Object;
            _mastermindGame.RulesEngineDI = _rulesEngineMock.Object;

            //Act
            var gameState = _mastermindGame.ProcessInput(inputValue);
            
            //Assert
            Assert.AreEqual(GameState.WrongGuess, gameState);
        }
    }
}
