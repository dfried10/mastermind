﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using Mastermind.Models.Game;
using Mastermind.Core.Engine;

namespace Mastermind.Tests.Mastermind.Core.Engine
{
    [TestFixture]
    public class RulesEngineTests
    {
        private RulesEngine _rulesEngine;
        private Code _guessCodeFake;
        private Code _masterCodeFake;

        [SetUp]
        public void Setup()
        {
            _rulesEngine = new RulesEngine();            
        }

        /*
         * Test Cases:
         *  1. Guess and MC are exact match, returns result
         *  2. MC digit repeats in two locations, one exact match, returns result
         *  3. Guess all correct digits in wrong place
         *  4. Guess and MC Don't match  
         *  5. Matches the example in assignment desc, don't add a fuzzy match
         *      when there is an exact match already
         *  6. Look for the case where there are duplicate number for a fuzzy match
         *      ex: Code: 1145 vs Guess: 2311, this sould return '--'
         *      ...I think, this is kind of an edge case
         */

        [TestCase]
        public void CompareGuessAgainstCode_GuessAndMCExactMatch_ReturnsFullMatchResult()
        {
            //Arrange
            var expectedMatches = 4;
            _guessCodeFake = GenerateFakeCode(1, 2, 3, 4);
            _masterCodeFake = GenerateFakeCode(1, 2, 3, 4);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);

            //Assert
            Assert.AreEqual(expectedMatches, result.NumberPositionMatches.Count);
        }

        [TestCase]
        public void CompareGuessAgainstCode_MCDigitRepeatsButOnly1ExactMatch_1ExactMatch1RelativeMatch()
        {
            //Arrange
            var expectedMatches = 1;
            _guessCodeFake = GenerateFakeCode(1, 1, 3, 5);
            _masterCodeFake = GenerateFakeCode(1, 2, 1, 4);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);

            //Assert
            Assert.AreEqual(expectedMatches, result.NumberPositionMatches.Count);
            Assert.AreEqual(expectedMatches, result.NumberMatches.Count);
        }

        [TestCase]
        public void CompareGuessAgainstCode_AllCorrectDigitsButInWrongLocation_Returns4NumberMatches()
        {
            //Arrange
            var expectedMatches = 4;
            _guessCodeFake = GenerateFakeCode(1, 3, 4, 5);
            _masterCodeFake = GenerateFakeCode(5, 1, 3, 4);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);            

            //Assert
            Assert.AreEqual(expectedMatches, result.NumberMatches.Count);
        }

        [TestCase]
        public void CompareGuessAgainstCode_NoValuesMatch_ReturnsEmptyMatchArrays()
        {
            //Arrange
            var expectedMatches = 0;
            _guessCodeFake = GenerateFakeCode(1, 1, 1, 1);
            _masterCodeFake = GenerateFakeCode(2, 2, 2, 2);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);

            //Assert
            Assert.AreEqual(expectedMatches, result.NumberMatches.Count);

        }

        [TestCase]
        public void CompareGuessAgainstCode_GuessDuplicates1ValueInCode_OnlyPrintsExactMatch()
        {
            //Arrange
            var expectedNumPosMatches = 3;
            var expectedNumMatches = 0;
            _guessCodeFake = GenerateFakeCode(1, 1, 3, 4);
            _masterCodeFake = GenerateFakeCode(1, 2, 3, 4);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);

            //Assert
            Assert.AreEqual(expectedNumPosMatches, result.NumberPositionMatches.Count);
            Assert.AreEqual(expectedNumMatches, result.NumberMatches.Count);
        }

        [TestCase]
        public void CompareGuessAgainstCode_DoubleFuzzyDuplicateBetweenGuessAndCode_Returns2NumberMatches()
        {
            //Arrange
            var expectedNumPosMatches = 0;
            var expectedNumMatches = 2;
            _guessCodeFake = GenerateFakeCode(1, 1, 3, 4);
            _masterCodeFake = GenerateFakeCode(5, 6, 1, 1);

            //Act
            var result = _rulesEngine.CompareGuessAgainstCode(_guessCodeFake, _masterCodeFake);

            //Assert
            Assert.AreEqual(expectedNumPosMatches, result.NumberPositionMatches.Count);
            Assert.AreEqual(expectedNumMatches, result.NumberMatches.Count);
        }

        // UTILITY
        private Code GenerateFakeCode(params int[] vals)
        {
            Code code = new Code();
            foreach (var val in vals) 
            {
                code.Digits.Add(val);
            }

            return code;
        }
    }
}