﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using Mastermind.Core.Engine;

namespace Mastermind.Tests.Mastermind.Core.Engine
{
    [TestFixture]
    public class InputParserTests
    {
        private InputParser _inputParser;
        
        [SetUp]
        public void Setup()
        {
            _inputParser = new InputParser();
        }

        /*
         * Test Cases:
         * IsInputValid:
         *  1. Given regualr valid input, returns true
         *  2. Given 5 digit input, returns false
         *  3. Given number with letters,  returns false
         *  
         * ParseLineForGuess:
         *  1. Given console line, adds to Digits of a new Code
         */

        [TestCase]
        public void IsInputValid_GivenValidInput_ReturnsTrue()
        {
            //Arrange
            var inputValue = "1234";
            
            //Act
            var result = _inputParser.IsInputValid(inputValue);

            //Assert
            Assert.IsTrue(result);
        }

        [TestCase]
        public void IsInputValid_Given5DigitInput_ReturnsFalse()
        {
            //Arrange
            var inputValue = "12345";

            //Act
            var result = _inputParser.IsInputValid(inputValue);
            
            //Assert
            Assert.IsFalse(result);
        }
        
        [TestCase]
        public void IsInputValid_GivenAlphaNumericString_ReturnsFalse()
        {
            //Arrange
            var inputValue = "13Ab";

            //Act
            var result = _inputParser.IsInputValid(inputValue);

            //Assert
            Assert.IsFalse(result);
        }

        [TestCase]
        public void ParseLineForGuess_GivenExpectedString_ReturnsNewGuess()
        {
            //Arrange
            var inputVal = "1234";

            //Act
            var guessCode = _inputParser.ParseLineForGuess(inputVal);

            //Assert
            Assert.AreEqual(inputVal.Length, guessCode.Digits.Count);
        }
    }
}
